> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4381

## Malachi Colson

### Assignment #2 Requirements:

*Two Parts:*

1. Recipe Application Screenshots
2. Chapter Questions (Chs 3, 4)

#### README.md file should include the following items:

* Screenshot of running application’s first user interface; 
* Screenshot of running application’s second user interface; 

#### Assignment Screenshots:

*Screenshot of Recipe App UI 1*:

![Recipe App UI 1](img/recipe_1.png)

*Screenshot of Recipe App UI 2*:

![Recipe App UI 2](img/recipe_2.png)