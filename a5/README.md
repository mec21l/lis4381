> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4381

## Malachi Colson

### Assignment #5 Requirements:

*Three Parts:*
 
1. Provide screenshots of failed validation
2. Provide screenshots of successful validation
3. Provide link to local web app

#### README.md file should include the following items:
 
* Screenshot of failed validation (Before);
* Screenshot of failed validation (After);
* Screenshot of successful validation (Before);
* Screenshot of successful validation (After); 

#### Assignment Screenshots:

*Screenshot of failed validation (before)*:

![Invalid_1](img/invalid_1.png)

*Screenshot of failed validation (after)*:

![Invalid_2](img/invalid_2.png)

*Screenshot of successful validation (before)*:

![Valid_1](img/valid_1.png)

*Screenshot of successful validation (after)*:

![Valid_2](img/valid_2.png)

#### Localhost Link:

*LocalHost:*
[A5 local web app Link](http://localhost:8080/repos/lis4381/index.php "LocalHost")