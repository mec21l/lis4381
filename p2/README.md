> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4381

## Malachi Colson

### Project #2 Requirements:

*Three Parts:*
 
1. Provide screenshots of cell editing
2. Provide screenshots of cell deletion
3. Provide link to local web app

#### README.md file should include the following items:
 
* Screenshot of homescreen;
* Screenshot of project 2 page;
* Screenshot of editing cell;
* Screenshot of failed server-side validation;
* Screenshot of successful server-side validation;
* Screenshot of cell deletion verification;
* Screenshot of successful cell deletion;

#### Assignment Screenshots:

*Screenshot of homepage*:

![Home](img/home.png)

*Screenshot of Project 2 page*:

![Index](img/index.png)

*Screenshot of edit table*:

![Edit](img/edit.png)

*Screenshot of failed validation*:

![Fail](img/fail.png)

*Screenshot of successful validation*:

![Pass](img/pass.png)

*Screenshot of error prevention*:

![Prompt](img/prompt.png)

*Screenshot of successful deletion*:

![Delete](img/delete.png)

*Screenshot of RSS*:

![RSS](img/rss.png)

#### Localhost Link:

*LocalHost:*
[A5 local web app Link](http://localhost:8080/repos/lis4381/index.php "LocalHost")