> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4381

## Malachi Colson

### Project #1 Requirements:

*Four Parts:*
 
1. Create a launcher icon image and display it in both activities (screens) 
2. Must add background color(s) to both activities 
3. Must add border around image and button 
4. Must add text shadow (button)

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1;
* Screenshot of running application’s first user interface; 
* Screenshot of running application’s second user interface; 

#### Assignment Screenshots:

*Screenshot of running application 1*:

![App Run](img/app1.png)

*Screenshot of running application 2*:

![App Run](img/app2.png)
