> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4381

## Malachi Colson

### Assignment #4 Requirements:

*Three Parts:*
 
1. Create Web Database
2. Provide screenshots of running localhost
3. Provide localhost link

#### README.md file should include the following items:
 
* Screenshot of web application's home page
* Screenshot of failed validation;
* Screenshot of successful validation;
* Link to local lis4381 web app; 

#### Assignment Screenshots:

*Screenshot of Homepage*:

![Home](img/home.png)

*Screenshot of failed validation*:

![Fail](img/fail.png)

*Screenshot of successful validation*:

![Pass](img/pass.png)


#### Localhost Link:

*LocalHost:*
[A4 local web app Link](http://localhost:8080/repos/lis4381/index.php "LocalHost")