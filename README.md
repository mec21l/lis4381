> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4381

## Malachi Colson

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Install JDK
    - Install Android Studio and create My First App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
    - Provide git command descriptions
2. [A2 README.md](a2/README.md "My A2 README.md file") 

    - Create Healthy Recipes Android app
    - Screenshot of running application’s first user interface 
    - Screenshot of running application’s second user interface 

3. [A3 README.md](a3/README.md "My A3 README.md file")

    - Create ERD based upon business rules
    - Provide screenshot of completed ERD
    - Provide DB resource links

4. [A4 README.md](a4/README.md "My A4 README.md file")

    - Create Web Database
    - Provide screenshots of running localhost
    - Provide localhost link

5. [A5 README.md](a5/README.md "My A5 README.md file")

    - Provide screenshots of failed validation
    - Provide screenshots of successful validation
    - Provide link to local web app

6. [P1 README.md](p1/README.md "My P1 README.md file")

    - Create My Business Card Android app
    - Screenshot of running application’s first user interface
    - Screenshot of running application’s second user interface

7. [P2 README.md](p2/README.md "My P2 README.md file")

    - Provide screenshots of cell editing
    - Provide screenshots of cell deletion
    - Provide link to local web app