> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Malachi Colson

### Assignment #1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1, 2)

#### README.md file should include the following items:

* Screenshot of AMPPS Installation [My PHP Installation](https://bitbucket.org/mec21l/lis4381/src/master/phpinfo.php "My PHP Installation");
* Screenshot of running java Hello;
* Screenshot of running Android Studio - My First App
* git commands w/short descriptions;
* Bitbucket repo links: a) this assignment and b) the completed tutorials above (bitbucketstationlocations and myteamquotes).

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Creates a new repository
2. git status - Displays the state of the working dirctory and the staging area
3. git add - Adds a change in the working directory to the staging area
4. git commit - Captures a snapshot of the project's currently staged changes
5. git push - Uploads local repository content to a remote repository
6. git pull - Downloads remote repository content to a local repository
7. git merge - Takes individual lines of development created by git branch and  integrates them into a single branch

#### Assignment Screenshots:

*Screenshot of AMPPS running*: [My PHP Installation](https://bitbucket.org/mec21l/lis4381/src/master/phpinfo.php "My PHP Installation")

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mec21l/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/mec21l/myteamquotes/ "My Team Quotes Tutorial")