> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4381

## Malachi Colson

### Assignment #3 Requirements:

*Three Parts:*
 
1. Create ERD based upon business rules
2. Provide screenshot of completed ERD
3. Provide DB resource links

#### README.md file should include the following items:

* Screenshot of ERD; 
* Screenshot of running application’s opening user interface; 
* Screenshot of running application’s processing user input;
* Screenshots of 10 records for each table—use "select *" from each table;
* Links to the following files: 
      a. a3.mwb 
      b. a3.sql 

#### Assignment Screenshots:

*Screenshot of ERD*:

![ERD](img/ERD.png)

*Screenshot of running application’s opening user interface*:

![App Run](img/app2.png)

*Screenshot of running application’s processing user input*:

![App Run 2](img/app.png)

*Screenshot of 10 records for each table—use "select *" from petstore table*:

![Petstore Table](img/petstore.png)

*Screenshot of 10 records for each table—use "select *" from customer table*:

![Customer Table](img/customer.png)

*Screenshot of 10 records for each table—use "select *" from pet table*:

![Pet Table](img/pet.png)

#### ERD Links:

*MWB File:*
[A3 ERD MWB File Link](https://bitbucket.org/mec21l/lis4381/src/master/a3/database/Model.mwb "ERD MWB File")

*SQL File:*
[A3 ERD SQL File Link](https://bitbucket.org/mec21l/lis4381/src/master/a3/database/a3.sql "ERD SQL File")